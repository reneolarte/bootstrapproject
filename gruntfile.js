module.exports = function (grunt) {
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });
    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        watch: {
            files: ['css/*.scss'],
            task: ['css']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './'
                    }
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'images',
                    src: ['images/*.{png,jpg,gif, jfif}'],
                    dest: 'dist'
                }]
            }
        },
        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './', //current working directory
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            images: {
                files: [{
                    expand: true,
                    cwd: './images', //current working directory
                    src: ['images/*.*'],
                    dest: 'dist'
                }]
            },
        },
        clean: {
            build: {
            src: ['dist/'] //clean the distribution folder
           }
        },
        cssmin: {
            dist: {}
        },
        uglify: {
            dist: {}
        },
        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release:{
                files: [{
                    src: [
                        'dist/css/*.css',
                        'dist/js/*.js'
                    ]
                }]
            }
            
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },
        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 'precios.html', 'nosotros.html', 'terminos.html', 'contacto.html']
            },
            options: {
                flow: {
                    steps: {
                    css: ['cssmin'],
                    js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block) {
                                    var generated = context.options.generated;
                                    generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                    }
                            }
                        }]
                    }
                }
           }
        },
        usemin: {
            html: ['dist/index.html', 'dist/terminos.html', 'dist/nosotros.html', 'dist/precios.html', 'dist/contacto.html'],
            options: {
            assetsDir: ['dist', 'dist/css', 'dist/js', 'dist/images']
           }
        }
    });

    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-filerev');
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync','watch']);
    grunt.registerTask('img:compress', ['imagemin']);

    grunt.registerTask('build', [
        'clean', //Borramos el contenido de dist
        'copy', //Copiamos los archivos html a dist
        'imagemin', //Optimizamos imagenes y las copiamos a dist
        'useminPrepare', //Preparamos la configuracion de usemin
        'concat',
        'cssmin',
        'uglify',
        'filerev', //Agregamos cadena aleatoria
        'usemin' //Reemplazamos las referencias por los archivos generados por filerev
    ]);
}