      $(function () {

        $('.carousel').carousel({
          interval: 2000
        })

        $('[data-toggle="popover"]').popover()
        $('[data-toggle="tooltip"]').tooltip()

        $('#exampleModal').on('show.bs.modal', function (e){
          console.log("el modal suscribirse se esta mostrando")
          $('#modalSuscribirse').removeClass('btn btn-primary col-sm-6')
          $('#modalSuscribirse').addClass('btn col-sm-6 btn-danger')
          $('#modalSuscribirse').prop('disabled', true)
        })
        $('#exampleModal').on('shown.bs.modal', function (e){
          console.log("el modal suscribirse se mostro")
        })
        $('#exampleModal').on('hide.bs.modal', function (e){
          console.log("el modal suscribirse se oculta")
        })
        $('#exampleModal').on('hidden.bs.modal', function (e){
          console.log("el modal suscribirse se oculto")
          $('#modalSuscribirse').prop('disabled', false)

        })
      })
